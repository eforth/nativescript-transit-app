var fileSystemModule = require('file-system');
var frameModule = require("ui/frame");
var Observable = require("data/observable").Observable;
var ObservableArray = require("data/observable-array").ObservableArray;

var documents = fileSystemModule.knownFolders.currentApp();
var jsonFile = documents.getFile('data/busTerminals.json');

var terminals = new ObservableArray([]);
var viewModel = new Observable();

viewModel.set("terminals", terminals);

function onLoad(args) {
	var page = args.object;
    page.bindingContext = viewModel;

	jsonFile.readText().then(function (content) {
	    try {
	        var jsonData = JSON.parse(content);
	        terminals.push(jsonData);
	    } catch (err) {
	        throw new Error('Could not parse JSON file');
	    }
	}, function (error) {
	    throw new Error('Could not read JSON file');
	});
}

function onListItemTap(args) {
	console.log(JSON.stringify(terminals.getItem(args.index)));
	frameModule.topmost().navigate({
		moduleName: "bus-terminal",
        context: {"terminal": terminals.getItem(args.index)}
	});
}

function onNavBtnTap() {
	frameModule.topmost().goBack();
}


exports.onLoad = onLoad;
exports.onListItemTap = onListItemTap;
exports.onNavBtnTap = onNavBtnTap;