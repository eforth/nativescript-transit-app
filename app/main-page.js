var frameModule = require("ui/frame");

exports.busTerminals = function () {
	frameModule.topmost().navigate("bus-terminals");
};

exports.about = function () {
	frameModule.topmost().navigate("about");
};