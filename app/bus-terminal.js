var googleMapsSDKModule = require("nativescript-google-maps-sdk");
var frameModule = require("ui/frame");
var Observable = require("data/observable").Observable;
var ObservableArray = require("data/observable-array").ObservableArray;


var terminals = new ObservableArray([]);
var viewModel = new Observable();


function onLoad(args) {
	var page = args.object;
	var terminal = page.navigationContext.terminal;

    viewModel.set("latitude", terminal.latitude);
    viewModel.set("longitude", terminal.longitude);
    viewModel.set("name", terminal.name);
    viewModel.set("zoom", 16); 
    viewModel.set("bearing", 0);
    viewModel.set("tilt", 0); 
    viewModel.set("padding", [40, 40, 40, 40]);

    page.bindingContext = viewModel;
}

function onMapReady(args) {
	var mapView = args.object;
	var marker = new googleMapsSDKModule.Marker();
	marker.position = googleMapsSDKModule.Position.positionFromLatLng(viewModel.get("latitude"), viewModel.get("longitude"));
	marker.title = viewModel.get("name");
	mapView.addMarker(marker);
}

function onNavBtnTap() {
	frameModule.topmost().goBack();
}


exports.onLoad = onLoad;
exports.onMapReady = onMapReady;
exports.onNavBtnTap = onNavBtnTap;